package cutlist

import (
	"fmt"

	"fmuehlis/smartmkv-go/logger"

	"gopkg.in/ini.v1"
)

type Cutlist struct {
	General CutlistGeneral
	Cuts    []Cut
	Info    CutlistInfo
	Meta    CutlistMeta
}

// FromFile reads the data from path to a cutlist struct
func (cutlist *Cutlist) FromFile(path string) error {
	file, err := ini.Load(path)
	if err != nil {
		logger.Error.Printf("Error loading ini: %v", err)
		return err
	}

	if err := file.MapTo(cutlist); err != nil {
		logger.Error.Printf("Error reading file to cutlist: %v", err)
		return err
	}

	// Read cuts to slice of "Cut"
	for i := 0; i < cutlist.General.NoOfCuts; i++ {
		cut := new(Cut)
		sec := file.Section(fmt.Sprintf("Cut%d", i))
		if err := sec.MapTo(&cut); err != nil {
			logger.Error.Printf("Error reading cut section: %v", err)
			return err
		}
		if cut.DurationFrames > 0 {
			logger.Debug.Printf("cut %d: %v", i, cut)
			cutlist.Cuts = append(cutlist.Cuts, *cut)
		}
	}
	return nil
}

type CutlistGeneral struct {
	Application                string
	Version                    string
	FramesPerSecond            float32
	DisplayAspectRatio         string
	IntendedCutApplicationName string
	IntendedCutApplication     string
	VDUseSmartRendering        bool
	Comment1                   string
	Comment2                   string
	NoOfCuts                   int
	ApplyToFile                uint
	OriginalFileSizeBytes      uint
}

type Cut struct {
	Start          float32 `ini:"Start"`
	StartFrame     int     `ini:"StartFrame"`
	Duration       float32 `ini:"Duration"`
	DurationFrames int     `ini:"DurationFrames"`
}

type CutlistInfo struct {
	Author                string
	RatingByAuthor        uint
	EPGError              bool
	ActualContent         string
	MissingBeginning      bool
	MissingEnding         bool
	MissingVideo          bool
	MissingAudio          bool
	OtherError            bool
	OtherErrorDescription string
	SuggestedMovieName    string
	UserComment           string
}

type CutlistMeta struct {
	CutlistId   uint
	GeneratedOn string
	GeneratedBy string
}
