package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"fmuehlis/smartmkv-go/cutlist"
	"fmuehlis/smartmkv-go/logger"

	// "github.com/dwbuiten/dumpindex/ffmsindex"
	ffmpeg_go "github.com/u2takey/ffmpeg-go"
	"gopkg.in/vansante/go-ffprobe.v2"
)

const (
	downloadFolder = "/home/fmuehlis/Downloads/"
	mediaFile      = downloadFolder + "Jujutsu_Kaisen_24.01.05_22-35_pro7maxx_30_TVOON_DE.mpg.HQ.avi"
	outputFile     = downloadFolder + "Jujutsu_Kaisen_24.01.05_22-35_pro7maxx_30_TVOON_DE.mpg.HQ.mkv"
	indexFile      = downloadFolder + "Jujutsu_Kaisen_24.01.05_22-35_pro7maxx_30_TVOON_DE.mpg.HQ.avi.ffindex_track00.kf.txt"
	cutlistFile    = downloadFolder + "Jujutsu_Kaisen_24.01.05_22-35_pro7maxx_30_TVOON_DE.mpg.HQ.avi.cutlist"
)

var (
	ErrInvalidCut      = errors.New("error invalid cut")
	ErrImpossibleSplit = errors.New("file cannot be split up properly")
)

var (
	audioSnippetName  = "audio_copy-00%d.mkv"
	copySnippetName   = "video_copy-00%d.mkv"
	encodeSnippetName = "video_encode-00%d.mkv"
)

type VideoSnippet struct {
	Encode   bool
	Start    int
	End      int
	FileName string
}

type AudioSnippet struct {
	Start     int
	End       int
	StartTime string
	EndTime   string
	FileName  string
}

func framesToTimeStamp(frames int, fps float32) string {
	seconds := float32(frames) / fps

	hours := int(seconds) / 3600
	minutes := (int(seconds) % 3600) / 60
	remainingSeconds := int(seconds) % 60

	milliseconds := int((seconds - float32(math.Floor(float64(seconds)))) * 1000)

	timestamp := fmt.Sprintf("%02d:%02d:%02d:%03d", hours, minutes, remainingSeconds, milliseconds)

	return timestamp
}

// keyframeBefore returns the closest keyframe to the left of frame
func keyframeBefore(frame int, keyframes []int) int {
	// TODO what if there is no keyframe before?
	start := 0
	end := len(keyframes)
	for start < end {
		mid := (start + end) / 2
		if keyframes[mid] < frame {
			start = mid + 1
		} else {
			end = mid
		}
	}
	logger.Debug.Printf("KEYFRAME: closest keyframe before %d is at %d", frame, keyframes[start-1])
	return keyframes[start-1]
}

// keyframeAfter returns the closest keyframe to the right of frame
func keyframeAfter(frame int, keyframes []int) int {
	// TODO what if there is no keyframe after?
	start := 0
	end := len(keyframes)
	for start < end {
		mid := (start + end) / 2
		if frame < keyframes[mid] {
			end = mid
		} else {
			start = mid + 1
		}
	}
	logger.Debug.Printf("KEYFRAME: closest keyframe after %d is at %d", frame, keyframes[end])
	return keyframes[end]
}

// isKeyFrame searches the list of keyframes for the frame
func isKeyFrame(frame int, keyframes []int) bool {
	start := 0
	end := len(keyframes)
	for start <= end {
		mid := (start + end) / 2
		if keyframes[mid] == frame {
			logger.Debug.Printf("KEYFRAME: found keyframe: %d", frame)
			return true
		} else if keyframes[mid] < frame {
			start = mid + 1
		} else if keyframes[mid] > frame {
			end = mid - 1
		}
	}
	return false
}

type CutManager struct {
	encodeCounter int
	copyCounter   int
	videoSnippets []VideoSnippet
	audioCounter  int
	audioSnippets []AudioSnippet
}

func (cutman *CutManager) AddAudioSnippet(start int, duration int, fps float32) {
	cutman.audioCounter++

	startTime := framesToTimeStamp(start, fps)

	end := start + duration
	endTime := framesToTimeStamp(end, fps)

	cutman.audioSnippets = append(cutman.audioSnippets, AudioSnippet{
		Start:     start,
		End:       end,
		StartTime: startTime,
		EndTime:   endTime,
		FileName:  fmt.Sprintf(audioSnippetName, cutman.audioCounter),
	})
}

func (cutman *CutManager) AddVideoSnippet(encode bool, start int, end int) {
	var filename string
	if encode {
		cutman.encodeCounter++
		logger.Debug.Printf("CUTMAN: encoding from %d to %d -> snippet %d", start, end, cutman.encodeCounter)
		filename = fmt.Sprintf(encodeSnippetName, cutman.encodeCounter)
	} else {
		cutman.copyCounter++
		logger.Debug.Printf("CUTMAN: copying from %d to %d -> snippet %d", start, end, cutman.copyCounter)
		filename = fmt.Sprintf(copySnippetName, cutman.copyCounter)
	}
	cutman.videoSnippets = append(cutman.videoSnippets, VideoSnippet{
		Encode:   encode,
		Start:    start,
		End:      end,
		FileName: filename,
	})
}

func (cutman *CutManager) VideoSnippetsToString() []string {
	var snippets []string
	for i, snippet := range cutman.videoSnippets {
		if i == 0 {
			snippets = append(snippets, downloadFolder+snippet.FileName)
		} else {
			snippets = append(snippets, "+"+downloadFolder+snippet.FileName)
		}
	}
	snippets = append(snippets, downloadFolder+"audio_copy.mkv")
	return snippets
}

func (cutman *CutManager) AudioSnippetsToString() string {
	var snippets []string
	for _, snippet := range cutman.audioSnippets {
		snippets = append(snippets, snippet.FileName)
	}
	return strings.Join(snippets, " ")
}

func (cutman *CutManager) AudioSnippetTimeToString() string {
	var snippets []string
	for _, snippet := range cutman.audioSnippets {
		snippets = append(snippets, fmt.Sprintf("%s-%s", snippet.StartTime, snippet.EndTime))
	}
	return strings.Join(snippets, ",+")
}

func (cutman *CutManager) SimulateSmartMerge(start int, duration int, keyframes []int) error {
	end := start + duration
	if end <= start {
		logger.Error.Printf("error endpoint %d is before startingpoint %d skipping...", end, start)
		return nil
	}

	logger.Debug.Printf("CUTMAN: processing cut: start=%d end=%d", start, end)

	if isKeyFrame(start, keyframes) {
		if isKeyFrame(end, keyframes) {
			logger.Debug.Print("CUTMAN: copying entire snippet keyframe to keyframe")
			cutman.AddVideoSnippet(false, start, end)
			return nil
		}
		encodeStart := keyframeBefore(end, keyframes)
		if encodeStart <= start {
			logger.Debug.Print("CUTMAN: encoding entire snippet")
			cutman.AddVideoSnippet(true, start, end)
			return nil
		}
		logger.Debug.Print("CUTMAN: copying from start to before keyframe, encoding the rest")
		cutman.AddVideoSnippet(
			false,
			start,
			encodeStart-1, // Keyframe - 1
		)
		cutman.AddVideoSnippet(
			true,
			encodeStart, // Keyframe
			end,
		)
		return nil
	}

	encodeEnd := keyframeAfter(start, keyframes)
	if end <= (encodeEnd - 1) {
		logger.Debug.Print("CUTMAN: encoding entire snippet")
		cutman.AddVideoSnippet(true, start, end)
		return nil
	}

	logger.Debug.Print("CUTMAN: encoding from start to before keyframe")
	cutman.AddVideoSnippet(
		true,
		start,
		encodeEnd-1, // Keyframe - 1
	)
	copyDuration := end - encodeEnd

	if copyDuration > 0 {
		logger.Debug.Printf("CUTMAN: searching best cut for the last %d frames", copyDuration)
		if err := cutman.SimulateSmartMerge(encodeEnd, copyDuration, keyframes); err != nil {
			return err
		}
		return nil
	}
	return nil
}

func cut(filePath string, cl *cutlist.Cutlist) {
	ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFn()

	mediaData, err := ffprobe.ProbeURL(ctx, filePath)
	if err != nil {
		logger.Error.Fatalf("Error getting data: %v", err)
	}

	// Create index file
	cmd := exec.Command("ffmsindex", "-f", "-p", "-k", mediaFile)
	if err := cmd.Run(); err != nil {
		logger.Error.Printf("Error executing ffmsindex: %v", err)
	}

	// Read in index file
	file, err := os.Open(indexFile)
	if err != nil {
		logger.Error.Printf("Error opening index file: %v", err)
		return
	}
	defer file.Close()

	// Convert lines to integer array
	var keyframes []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		// Ignore lines that dont start with a number
		if line[0] >= '0' && line[0] <= '9' {
			keyframe, err := strconv.Atoi(line)
			if err != nil {
				logger.Error.Printf("Error reading keyframe to int: %v", err)
				return
			}
			keyframes = append(keyframes, keyframe)
		}
	}

	// Extract video stream data
	// Need: RFrameRate or AvgFrameRate?, DisplayApectRatio, SampleAspectRatio, NbFrames
	video := mediaData.FirstVideoStream()
	logger.Debug.Print(video)
	// audio := mediaData.StreamType(ffprobe.StreamAudio)

	cutman := new(CutManager)
	for i := range cl.Cuts {
		cutman.AddAudioSnippet(cl.Cuts[i].StartFrame, cl.Cuts[i].DurationFrames, cl.General.FramesPerSecond)

		if err := cutman.SimulateSmartMerge(cl.Cuts[i].StartFrame, cl.Cuts[i].DurationFrames, keyframes); err != nil {
			logger.Error.Printf("Error simulating SmartMkvMerge: %v", err)
			return
		}
		logger.Debug.Println("")
	}

	// Cut audio TODO if ac3 exists cut it here instead of original audio
	cmd = exec.Command("mkvmerge", "--ui-language", "en_US", "-D", "--split", "parts:"+cutman.AudioSnippetTimeToString(), "-o", downloadFolder+"audio_copy.mkv", mediaFile)
	logger.Debug.Print(cmd.String())
	if err := cmd.Run(); err != nil {
		logger.Error.Printf("error splitting audio: %v", err)
	}

	var copySnippets []string
	for _, smartCut := range cutman.videoSnippets {
		if smartCut.Encode {
			// Directly encode the snippets
			logger.Debug.Printf("%s: encoding from %d to %d", smartCut.FileName, smartCut.Start, smartCut.End)
			err := ffmpeg_go.
				Input(mediaFile, ffmpeg_go.KwArgs{
					"ss": float32(smartCut.Start) / cl.General.FramesPerSecond,
				}).
				Output(downloadFolder+smartCut.FileName, ffmpeg_go.KwArgs{
					"vframes": smartCut.End - smartCut.Start,
					"vf":      fmt.Sprintf("setsar=%s", strings.Replace(video.SampleAspectRatio, ":", "/", 1)),
					"an":      "",
					"sn":      "",
					"dn":      "",
					"bsf:v":   "h264_mp4toannexb,dump_extra=keyframe,trace_headers",
				}).OverWriteOutput().Run()
			if err != nil {
				logger.Error.Printf("error encoding file %s: %v", smartCut.FileName, err)
			}
		} else {
			logger.Debug.Printf("%s: copying from %d to %d", smartCut.FileName, smartCut.Start, smartCut.End)
			copySnippets = append(copySnippets, fmt.Sprintf("%d-%d", smartCut.Start, smartCut.End))
		}
	}

	// Cut copyable snippets out of recording
	cmd = exec.Command("mkvmerge", "--ui-language", "en_US", "-A", "--split", "parts-frames:"+strings.Join(copySnippets, ","), "-o", downloadFolder+"video_copy.mkv", mediaFile)
	logger.Debug.Print(cmd.String())
	if err := cmd.Run(); err != nil {
		logger.Error.Printf("error splitting frames: %v", err)
	}

	commandStrings := []string{
		"--engage", "no_cue_duration",
		"--engage", "no_cue_relative_position",
		"--ui-language", "en_US",
		"-o", outputFile,
	}
	commandStrings = append(commandStrings, cutman.VideoSnippetsToString()...)
	logger.Debug.Print(commandStrings)
	cmd = exec.Command("mkvmerge", commandStrings...)
	logger.Debug.Print(cmd.String())
	if err := cmd.Run(); err != nil {
		logger.Error.Printf("error merging files: %v", err)
	}
}

func main() {
	// Test reading out cutlist
	cl := new(cutlist.Cutlist)
	if err := cl.FromFile(cutlistFile); err != nil {
		logger.Debug.Printf("Error reading cutlist from file: %v", err)
	}

	// Test reading out video file data
	/*video := new(media.VideoFile)
	video.FromFile("/home/fmuehlis/Downloads/Jujutsu_Kaisen_24.01.05_22-35_pro7maxx_30_TVOON_DE.mpg.HQ.avi")*/

	cut(mediaFile, cl)
}
