module fmuehlis/smartmkv-go

go 1.21.4

require (
	github.com/aws/aws-sdk-go v1.38.20 // indirect
	github.com/dwbuiten/dumpindex v4.0.0+incompatible // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/u2takey/ffmpeg-go v0.5.0 // indirect
	github.com/u2takey/go-utils v0.3.1 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/vansante/go-ffprobe.v2 v2.1.1 // indirect
)
