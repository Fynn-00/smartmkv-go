package logger

import (
	"log"
	"os"
)

var (
	flags = log.LstdFlags | log.Lshortfile
	Debug = log.New(os.Stdout, "DEBUG: ", flags)
	Info  = log.New(os.Stdout, "INFO: ", flags)
	Warn  = log.New(os.Stdout, "WARNING: ", flags)
	Error = log.New(os.Stdout, "ERROR: ", flags)
)
