package media

import (
	"context"

	"gopkg.in/vansante/go-ffprobe.v2"
)

type VideoFile struct {
	Framerate float32
	Dar       string
	Sar       string
	Frames    uint
	Ac3Index  uint
}

func (file *VideoFile) FromFile(path string) error {
	data, err := ffprobe.ProbeURL(context.Background(), path)
	if err != nil {
		return err
	}
	file.Frames = data.FirstVideoStream().NbFrames
	return nil
}
